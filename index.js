/* eslint-disable require-jsdoc */
const http = require('http');
const configs = require('./bin/infra/configs/global_config');
const logger = require('./bin/helpers/utils/logger');
const port = process.env.port || configs.get('/port') || 1337;
const debug = require('debug')('myapp:server');
const project = require('./package.json');
const app = require('./bin/app/server');


function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }
  switch (error.code) {
    case 'EACCES':
      process.exit(1);
      break;
    case 'EADDRINUSE':
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

app.set('port', port);

const server = http.createServer(app);

// eslint-disable-next-line max-len
logger.log('app-listen', `${project.name} started, running version ${project.version} listening at ${port}`);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

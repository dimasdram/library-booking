// const express = require('express');
const app = require('../../bin/app/server');
const request = require('supertest');

describe('LibraryRouteTest integration test', ()=>{
  describe('findBook', ()=>{
    it('response success findbook', function(done) {
      request(app)
          .get('/library/v1/library?genre=love')
          .set('Accept', 'application/json')
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
    });
    it('error without genre findbook', function(done) {
      request(app)
          .get('/library/v1/library?genre=')
          .set('Accept', 'application/json')
          .expect(400)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
    });
  });
});

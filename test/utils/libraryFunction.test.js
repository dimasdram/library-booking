const libraryFunction = require('../../bin/utils/libraryFunction');
const axios = require('../../bin/utils/axios');
const sinon = require('sinon');

describe('libraryFunction', ()=>{
  describe('findAuthor', ()=>{
    it('Success mapping author name', ()=>{
      sinon.stub(axios, 'get').resolves({name: 'test'});
      libraryFunction.findAuthor([{
        author: {key: 'author/dimastest123'},

      }]);
      axios.get.restore();
    });
  });

  describe('findBookByKey', ()=>{
    it('catch error attribute function is null', () => {
      sinon.stub(axios, 'get').resolves({});
      libraryFunction.findBookByKey('testBookKey');
      axios.get.restore();
    });
  });
});

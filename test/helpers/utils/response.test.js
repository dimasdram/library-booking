const response = require('../../../bin/helpers/utils/response');

describe('Utils Response', ()=>{
  const res = {
    send: function() {},
    json: function(d) {},
    status: function(s) {
      this.statusCode = s;
      return this;
    },
  };
  describe('successPagination', ()=>{
    it('Success successPagination', ()=>{
      response.successPagination( res, 200, {}, {}, 'message');
    });
    it('Success successPagination without code and message', ()=>{
      response.successPagination( res, null, {}, {}, null);
    });
  });
  describe('success', ()=>{
    it('Success success without code', ()=>{
      response.success( res, null, {}, 'message');
    });
  });
  describe('successPagination', ()=>{
    it('Success successPagination without code', ()=>{
      response.error( res, null, 'message');
    });
  });
});

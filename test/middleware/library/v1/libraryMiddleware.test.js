const middleware = require(
    '../../../../bin/middleware/library/v1/libraryMiddleware',
);

const res = {
  send: function() {},
  json: function(d) {},
  status: function(s) {
    this.statusCode = s;
    return this;
  },
};

const req = {
  body: {
    bookKey: '/works/OL21177W',
    coverEdition: 'testCover',
    name: 'testName',
    address: 'testAdress',
    pickUpTime: 'testTime',
    duration: 0,
  },
};

describe('libraryMiddleware', ()=>{
  describe('validateFindBook', ()=>{
    it('catch error if validateFindBook dont have attribute', ()=>{
      middleware.validateFindBook(null, null, null);
    });
  });
  describe('validateBookingInput', ()=>{
    it('Success validateBookingInput', ()=>{
      middleware.validateBookingInput(req, res);
    });
    it('fail all object validation validateBookingInput', ()=>{
      const req = {body: {}};
      middleware.validateBookingInput(req, res);
    });
  });
});

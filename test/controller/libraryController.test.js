const sinon = require('sinon');
const controller = require(
    '../../bin/controllers/library/v1/libraryController',
);
const libraryFunction = require(
    '../../bin/utils/libraryFunction',
);
const storage = require(
    '../../bin/helpers/storage/arrayStorage',
);


const req = {
  body: {},
  params: {},
  query: {},
  headers: {},
};
const res = {
  send: function() {},
  json: function(d) {},
  status: function(s) {
    this.statusCode = s;
    return this;
  },
};


describe('LibraryController', ()=>{
  describe('findBook', ()=>{
    it('book not found', ()=>{
      // validate if findBookByGenre() return null
      sinon.stub(libraryFunction, 'findBookByGenre').resolves(null);
      controller.findBook(req, res);
      libraryFunction.findBookByGenre.restore();
    });

    it('Success to find book', ()=>{
      // validate if findBookByGenre() return data
      const data = {
        key: 'testKey',
        subject: 'love',
        ia_collection: 'test',
        title: 'testBook',
        edition: 1,
        cover_edition_key: 'editiontest1',
      };
      sinon.stub(libraryFunction, 'findBookByGenre').resolves({works: [data]});
      controller.findBook(req, res);
      libraryFunction.findBookByGenre.restore();
    });

    it('catch error findBook', ()=>{
      // if req and res set null
      const req = null;
      const res = null;
      controller.findBook(req, res);
    });
  });


  describe('booking', ()=>{
    it('catch error booking', ()=>{
      // if req and res set null
      const req = null;
      const res = null;
      controller.booking(req, res);
    });

    const req = {
      body: {
        bookKey: '/works/OL21177W',
        coverEdition: 'testCover',
        name: 'testName',
        address: 'testAdress',
        pickUpTime: 'testTime',
        duration: 0,
      },
    };

    const dataFindBook = {
      authors: {key: 'dimas'},
      title: 'this is testing data',
    };

    it('success booking', ()=>{
      dataFindBook.description= {value: 'description with key value'};
      const dataFindAuthor = ['Dimas'];

      sinon.stub(libraryFunction, 'findBookByKey').resolves(dataFindBook);
      sinon.stub(libraryFunction, 'findAuthor').resolves(dataFindAuthor);
      controller.booking(req, res);
      libraryFunction.findBookByKey.restore();
      libraryFunction.findAuthor.restore();
    });

    it('success booking with value on descriptions', ()=>{
      dataFindBook.description= 'description with key value';
      const dataFindAuthor = ['Dimas'];

      sinon.stub(libraryFunction, 'findBookByKey').resolves(dataFindBook);
      sinon.stub(libraryFunction, 'findAuthor').resolves(dataFindAuthor);
      controller.booking(req, res);
      libraryFunction.findBookByKey.restore();
      libraryFunction.findAuthor.restore();
    });

    it('success booking with descriptions is null', ()=>{
      dataFindBook.description= '';
      const dataFindAuthor = ['Dimas'];

      sinon.stub(libraryFunction, 'findBookByKey').resolves(dataFindBook);
      sinon.stub(libraryFunction, 'findAuthor').resolves(dataFindAuthor);
      controller.booking(req, res);
      libraryFunction.findBookByKey.restore();
      libraryFunction.findAuthor.restore();
    });

    it('Fail booking author not found', ()=>{
      sinon.stub(libraryFunction, 'findBookByKey').resolves(true);
      sinon.stub(libraryFunction, 'findAuthor').resolves(null);
      controller.booking(req, res);
      libraryFunction.findBookByKey.restore();
      libraryFunction.findAuthor.restore();
    });

    it('Fail booking book not found', ()=>{
      sinon.stub(libraryFunction, 'findBookByKey').resolves(null);
      controller.booking(req, res);
      libraryFunction.findBookByKey.restore();
    });
  });

  describe('Booking List', ()=>{
    it('Success get book list', ()=>{
      sinon.stub(storage, 'findAll').resolves([]);
      controller.bookingList(req, res);
      storage.findAll.restore();
    });
  });
});

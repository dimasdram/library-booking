require('dotenv').config();
const confidence = require('confidence');

const config = {
  port: parseInt(process.env.PORT),
  openLibrary: {
    url: process.env.OPEN_LIBRARY_URL,
  },
};

const store = new confidence.Store(config);
exports.get = (key) => store.get(key);

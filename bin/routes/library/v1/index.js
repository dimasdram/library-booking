/* eslint-disable new-cap */
const express = require('express');

const Library = require('./library.route');

const router = express.Router();
router.use('/library', Library);

module.exports = router;

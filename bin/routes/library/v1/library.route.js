/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();
const controller = require('../../../controllers/library/v1/libraryController');
const {validateFindBook, validateBookingInput} = require(
    '../../../middleware/library/v1/libraryMiddleware',
);
router.get('/', validateFindBook, controller.findBook);
router.post('/booking', validateBookingInput, controller.booking);
router.get('/booking/list', validateBookingInput, controller.bookingList);

module.exports = router;

const axios = require('./axios');
const logger = require('../helpers/utils/logger');
const config = require('../infra/configs/global_config');

const scopeLog = 'catch error library function';

const findAuthor = async (arrayInput) => {
  const ctx = 'findAuthor';
  try {
    listAuthor = [];
    for (let i = 0; i< arrayInput.length; i++) {
      const author = arrayInput[i].author.key;
      const url = `${config.get('/openLibrary').url}${author}.json`;
      const findAuthor = await axios.get(
          url,
      );
      listAuthor.push(findAuthor.name);
    };
    return listAuthor;
  } catch (error) {
    logger.log(ctx, error, scopeLog);
    return null;
  }
};

const findBookByGenre = async (genre) => {
  const url = `${config.get('/openLibrary').url}/subjects/${genre}.json`;
  const findBook = await axios.get(
      url,
  );
  return findBook;
};

const findBookByKey = async (bookKey)=>{
  const url = `${config.get('/openLibrary').url}${bookKey}.json`;
  const findBook = await axios.get(
      url,
  );
  return findBook;
};

module.exports = {
  findAuthor,
  findBookByGenre,
  findBookByKey,
};

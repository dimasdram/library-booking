const axios = require('axios');

const defaultHeaders = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
};


const get = async (url, params, headers) =>{
  return new Promise((resolve, reject) => {
    axios.request({
      url,
      method: 'GET',
      headers: {
        ...defaultHeaders,
        ...headers,
      },
    }).then((response) => {
      resolve(response.data);
    }).catch((error) => {
      reject(error);
      return null;
    });
  });
};

module.exports = {
  get,
};

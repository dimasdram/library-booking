const Express = require('express');
const BodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');
const cors = require('cors');
const morgan = require('morgan');

// routes
const Library = require('../routes/library/v1/index');

// eslint-disable-next-line new-cap
const app = Express();
const path = require('path');


app.use(BodyParser.raw({
  inflate: true,
  limit: '50mb',
  type: 'application/octet-stream',
}));
app.use(Express.json());
app.use(BodyParser.json({limit: '50mb', extended: false}));
app.use(BodyParser.urlencoded({limit: '50mb', extended: false}));
app.use(cookieParser());
app.use(Express.static(path.join(__dirname, 'assets')));
app.use(cors());
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(morgan('dev'));

app.use('/library/v1', Library);
// app.use('/', 'Hello, Welcome to Library booking apps');

// catch 404 and forward to error handler
app.use((req, res) => {
  res.status(404).json({
    message: 'Not Found',
    statusCode: 404,
    requested_url: req.path,
  });
});

// error handler
app.use((err, req, res) => {
  res.status(err.status).json({
    message: 'Something Went Wrong on Our Side',
    requested_url: req.path,
  });
});

app.use((req, res) => {
  res.header('Access-Control-Allow-Origin',
      `${Config.nginx.host}:${Config.nginx.port}`);
  res.header('Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Secret, token');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Content-Type', 'application/json');
});

module.exports = app;

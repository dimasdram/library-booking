const bookingList = [];

const createBooking = (input) =>{
  bookingList.push(input);
  return bookingList;
};

const findListByName = (input) => {
  const result = bookingList.filter((key) => {
    return key.name === input;
  });
  return result;
};

const findAll = () => {
  return bookingList;
};

module.exports = {
  createBooking,
  findListByName,
  findAll,
};

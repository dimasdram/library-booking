/* eslint-disable new-cap */
const winston = require('winston');

const logger = new winston.createLogger({
  transports: [
    new winston.transports.Console({
      level: 'info',
      handleExceptions: true,
      json: false,
      colorize: true,
    }),
  ],
  exitOnError: false,
});

const log = (context, message, scope) => {
  const obj = {
    context,
    scope,
    message: `${message}`,
  };
  logger.info(obj);
};

module.exports = {
  log,
};


/* eslint-disable no-new-object */
const success = (res, code, data, message)=>{
  return res.status( code ).send({
    success: true,
    data,
    message: message,
    code: code || 200,
  });
};

const successPagination = (res, code, meta, data, message)=>{
  return res.status(code).send({
    success: true,
    data: data,
    meta: meta,
    message: message || '',
    code: code || 200,
  });
};

const error = (res, code, message)=>{
  return res.status(code).send({
    success: false,
    data: null,
    message: message,
    code: code || 500,
  });
};

module.exports = {
  success,
  error,
  successPagination,
};

const Joi = require('@hapi/joi');
const response= require('../../../helpers/utils/response');

const validateFindBook = async (req, res, next) => {
  try {
    const schema = Joi.object({
      genre: Joi.string().required(),
    });
    const {error} = schema.validate(req.query);

    if (error) {
      return response.error(res, 400, error.details[0].message);
    }
    return next();
  } catch (err) {
    return response.error(res, 'joi error', 'validateId');
  }
};

const validateBookingInput = async (req, res, next) => {
  try {
    const schema = Joi.object({
      bookKey: Joi.string().required(),
      coverEdition: Joi.string().required(),
      name: Joi.string().required(),
      address: Joi.string().required(),
      pickUpTime: Joi.string().required(),
      duration: Joi.number().required(),
    });
    const {error} = schema.validate(req.body);

    if (error) {
      return response.error(res, 400, error.details[0].message);
    }
    return next();
  } catch (err) {
    return response.error(res, 'joi error', 'validateId');
  }
};

module.exports = {
  validateFindBook,
  validateBookingInput,
};

const response = require('../../../helpers/utils/response');
const storage = require('../../../helpers/storage/arrayStorage');
const libraryFunction = require('../../../utils/libraryFunction');
const logger = require('../../../helpers/utils/logger');

const scopeLog = 'Catch error library controller';

const findBook = async (req, res) =>{
  const ctx = 'findBook';
  try {
    const {genre} = req.query;

    const findBook = await libraryFunction.findBookByGenre(genre);
    if (!findBook) {
      return response.error(res, 409, 'Fail to find book');
    }

    const bookData = findBook.works.map((data) => {
      data.subject = undefined;
      data.ia_collection = undefined;
      return {
        ...data,
      };
    });

    const responseMapping = {
      key: findBook.key,
      subject: genre,
      bookData,
    };

    return response.success(res, 200, responseMapping, 'Success to find Book');
  } catch (error) {
    logger.log(ctx, error, scopeLog);
    return response.error(res, 409, 'Fail to find book');
  }
};

const booking = async (req, res) =>{
  const ctx = 'findBook';
  try {
    const {
      bookKey,
      coverEdition,
      name,
      address,
      pickUpTime,
      duration,
    } = req.body;

    const findBook = await libraryFunction.findBookByKey(bookKey);
    if (!findBook) {
      return response.error(res, 404, 'Book not found');
    }

    const findAuthor = await libraryFunction.findAuthor(findBook.authors);
    if (!findAuthor) {
      return response.error(res, 404, 'Author not found');
    }

    const dataBooking = {
      title: findBook.title,
      authorName: findAuthor,
      edition: coverEdition,
      information: findBook.description.value? findBook.description.value:
        findBook.description?findBook.description:'',
      name,
      address,
      pickUpTime,
      duration,
    };
    storage.createBooking(dataBooking);
    return response.success(res, 200, dataBooking, 'Success to booking book');
  } catch (error) {
    logger.log(ctx, error, scopeLog);
    return response.error(res, 409, 'fail booking');
  }
};

const bookingList = async (req, res) =>{
  const list = storage.findAll();
  return response.success(res, 200, list, 'Success to booking book');
};

module.exports = {
  findBook,
  booking,
  bookingList,
};
